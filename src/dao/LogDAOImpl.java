package dao;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import domain.LogInfo;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author viktor
 */
public class LogDAOImpl implements LogDAO {

    @Override
    public Integer AddLogInfo(LogInfo li) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(li);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return li.getId_generate();
    }

    @Override
    public void UpdateLogInfo(LogInfo li) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(li);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public LogInfo getLogInfo(Integer id_generate) {
        Session session = null;
        // 
        LogInfo li = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from LogInfo where id_generate=:id_generate");
            query.setInteger("id_generate", id_generate);
            
            List<LogInfo> l_log = new ArrayList<LogInfo>();
            l_log = (List<LogInfo>) query.list();
            if (l_log.size() > 0) {
                
                li = l_log.get(0);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return li;
    }

    @Override
    public List<LogInfo> getAllListLog() {
        Session session = null;
        List<LogInfo> l_log = new ArrayList<LogInfo>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from LogInfo");
            
            l_log = (List<LogInfo>) query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return l_log;
    }
    
}
