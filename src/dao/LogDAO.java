package dao;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import domain.LogInfo;
import java.util.List;

/**
 *
 * @author viktor
 */
public interface LogDAO {
    public Integer AddLogInfo(LogInfo li);
    public void UpdateLogInfo(LogInfo li);
    public LogInfo getLogInfo(Integer id_generate);
    public List<LogInfo> getAllListLog();
}
