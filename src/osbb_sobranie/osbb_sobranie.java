package osbb_sobranie;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ScheduledExecutorService;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.simple.parser.ParseException;

import dao.Factory;
import domain.LogInfo;
import domain.Sobranie;
import service.SobranieService;

public class osbb_sobranie {
	
	
	public static void main(String[] args) throws IOException, FileNotFoundException, ParseException, ParserConfigurationException, TransformerException, TransformerConfigurationException, InvalidFormatException, SQLException {
		osbb_sobranie prs_site = new osbb_sobranie();

        Date date = new Date();
        try {
            /// ------------------------ Program Menu ----------------------- ///
            boolean bl_menu = true;
            while (bl_menu) {
                Integer menu_pos = 0;
                Scanner input = new Scanner(System.in);
                System.out.print("Выберите команду, которую необходимо выполнить:\n0. Загрузка реестр голосов\n6. Завершение приложения\n(Введите 0-6):  ");
                String str_menu = input.nextLine().trim();
                LogInfo li = new LogInfo();
                li.setDate_start(date);
                Integer gen_id = 0;
                switch (str_menu) {
                    case "0": {
                        li.setStart_step(0);
                        li.setStatus_cutent_state("no");
                        //li.setStatus_cutent_state(str_submenu);
                        gen_id = Factory.getInstance().getLogDAO().AddLogInfo(li);
                        break;
                    }
    
                    case "6": {
                        bl_menu = false;
                        break;
                    }
                    default: {
                        bl_menu = false;
                        break;
                    }
                }

                if (gen_id > 0) {
                    prs_site.StartParsingSite(gen_id);
                }
            }
        } catch (Exception e) {
        	
        }
    }
	
	private void StartParsingSite(Integer generation_id) throws IOException, FileNotFoundException, ParseException, ParserConfigurationException, TransformerException, TransformerConfigurationException, InvalidFormatException {
        LogInfo li = Factory.getInstance().getLogDAO().getLogInfo(generation_id);
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();

            if (li.getStart_step() == 0) {
                System.out.println("");
                System.out.println("--------- Старт импорта Голосов с XLSX файла - " + dateFormat.format(date));
                SobranieService gl = new SobranieService();
                li = gl.GuideDiscount(li);
                gl = null;
                date = new Date();
                System.out.println("");
                System.out.println("--------- Завершение импорта Голосов с XLSX файла - " + dateFormat.format(date));
                li = this.updateStep(0, li);
            }

        } catch (SocketTimeoutException e) {
            System.out.println("");
            System.out.println("--------- Ошибка в соединении, продолжение импорта Каталогов с места последнего разрыва ----------");
            Date dt1 = new Date();
            li.setDate_update(dt1);
            li.setError_msg(e.toString());
            Factory.getInstance().getLogDAO().UpdateLogInfo(li);

            this.StartParsingSite(generation_id);
        } catch (UnknownHostException e) {
            System.out.println("");
            System.out.println("--------- Ошибка в доступе к серверу, продолжение импорта Каталогов с места последнего разрыва ----------");
            Date dt1 = new Date();
            li.setDate_update(dt1);
            li.setError_msg(e.toString());
            Factory.getInstance().getLogDAO().UpdateLogInfo(li);

            this.StartParsingSite(generation_id);
        } catch (Exception e) {
            System.out.println("");
            System.out.println("--------- Ошибка в программе  ----------");
            Date dt1 = new Date();
            li.setStatus_cutent_state("error");
            li.setDate_update(dt1);
            li.setError_msg(e.toString());
            Factory.getInstance().getLogDAO().UpdateLogInfo(li);
            System.out.println(e.toString());
            this.StartParsingSite(generation_id);
        }
    }

    private LogInfo updateStep(Integer curent_step, LogInfo li) {
        Date date = new Date();
        if (li.getStart_step() < 5) {
            li.setCurent_step(curent_step);
            li.setDate_update(date);
            li.setDate_finish(date);
            li.setIs_finish(Boolean.TRUE);
        } else {
            li.setCurent_step(curent_step + 1);
            li.setDate_update(date);
            li.setStatus_cutent_state("yes");
            if (curent_step == 4 || curent_step == 7) {
                li.setDate_finish(date);
                li.setIs_finish(Boolean.TRUE);
            }
        }
        Factory.getInstance().getLogDAO().UpdateLogInfo(li);
        return li;
    }
}
