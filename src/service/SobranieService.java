package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hpsf.Decimal;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.json.simple.parser.ParseException;

import dao.Factory;
import domain.LogInfo;
import util.ProgrammSetting;

public class SobranieService {
	private ProgrammSetting ps = new ProgrammSetting();

    public LogInfo GuideDiscount(LogInfo li) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        File file = new File(ps.LoadCatalogOnLocalFile().get("folder_path") + "/" + ps.LoadCatalogOnLocalFile().get("file_ods"));
        this.readODS(file);

        //// ------ Log -------- ///
        Date dt = new Date();
        // li.setStatus_cutent_state("no");
        li.setDate_update(dt);
        Factory.getInstance().getLogDAO().UpdateLogInfo(li);

        file.exists();
        return li;
    }

    private void readODS(File file) throws IOException, InvalidFormatException {
    	SpreadSheet spreadsheet;
        try {
        	spreadsheet = SpreadSheet.createFromFile(file);
        	/////////////// --- CreateXML Node ////////////////////
        	int iLastRow2 = spreadsheet.getSheet("Лист2").getRowCount();
            int iLastColl2 = spreadsheet.getSheet("Лист2").getColumnCount();
            int flat_column=1, m2_column=3, question_columt=7, nasobr_columt=5;
            String name_col="";
            System.out.println(";;ЗА, м2;ПРОТИ, м2;ПУСТО, м2;ВСЬОГО, м2;;ЗА, к-сть;ПРОТИ, к-сть;ПУСТО, к-сть, к-сть;ВСЬОГО, к-сть;;ЗА %, м2;ПРОТИ %, м2;ПУСТО %, м2;ВСЬОГО %, м2;;ЗА %, к-сть;ПРОТИ %, к-сть;ПУСТО %, к-сть;ВСЬОГО %, к-сть");
            for(int j=question_columt; j<=iLastColl2; j++) {
            	Integer za_cnt=0, protiv_cnt=0, empty_count=0, za_nasobrcnt=0, protiv_nasobrcnt=0, empty_nasobrcnt=0, cnt_all=0;
            	Double za_m2=0.0, protiv_m2=0.0, empty_m2=0.0, za_sobrm2=0.0, protiv_sobrm2=0.0, empty_sobrm2=0.0, sum_all=0.0;
            	if (spreadsheet.getSheet("Лист2").getCellAt(question_columt, 1).getTextValue().length() == 0) {
            		break;
            	}
            	for(int i=1; i<=iLastRow2; i++) {
            		if (spreadsheet.getSheet("Лист2").getCellAt(0, i).getTextValue().length() == 0) {
                        question_columt++;
                        break;
            		}
            		if(i==1) {
            			name_col=spreadsheet.getSheet("Лист2").getCellAt(question_columt, i).getTextValue();                        
            		}
            		else {
            			if(spreadsheet.getSheet("Лист2").getCellAt(question_columt, i).getTextValue().indexOf("за")>=0) { 
            				if(spreadsheet.getSheet("Лист2").getCellAt(nasobr_columt, i).getTextValue().indexOf("+")>=0) {
            					za_nasobrcnt++;
            					za_sobrm2=za_sobrm2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            				else {
            					za_cnt++;
	            				za_m2=za_m2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            			}
            			if(spreadsheet.getSheet("Лист2").getCellAt(question_columt, i).getTextValue().indexOf("проти")>=0) { 
            				if(spreadsheet.getSheet("Лист2").getCellAt(nasobr_columt, i).getTextValue().indexOf("+")>=0) {
            					protiv_nasobrcnt++;
            					protiv_sobrm2=protiv_sobrm2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            				else {
	            				protiv_cnt++; 
	            				protiv_m2=protiv_m2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            			}
            			if(spreadsheet.getSheet("Лист2").getCellAt(question_columt, i).getTextValue().length()==0) { 
            				if(spreadsheet.getSheet("Лист2").getCellAt(nasobr_columt, i).getTextValue().indexOf("+")>=0) {
            					empty_nasobrcnt++;
            					empty_sobrm2=empty_sobrm2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            				else {
	            				empty_count++; 
	            				empty_m2=empty_m2+this.getSum(spreadsheet.getSheet("Лист2").getCellAt(m2_column, i).getTextValue());
            				}
            			}
            		}
            	}
            	
            	NumberFormat df = DecimalFormat.getInstance(Locale.FRENCH);
                df.setMinimumFractionDigits(2);
                df.setMaximumFractionDigits(2);            	
                cnt_all=za_cnt+protiv_cnt+empty_count+za_nasobrcnt+protiv_nasobrcnt+empty_nasobrcnt;
                sum_all=za_m2+protiv_m2+empty_m2+za_sobrm2+protiv_sobrm2+empty_sobrm2;
                System.out.print(name_col+";НА ЗБОРАХ;"+String.valueOf(df.format(za_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(protiv_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(empty_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(za_sobrm2+protiv_sobrm2+empty_sobrm2).replaceAll(" ", ""))+";;"+String.valueOf(za_nasobrcnt)+";"+String.valueOf(protiv_nasobrcnt)+";"+String.valueOf(empty_nasobrcnt)+";"+String.valueOf(za_nasobrcnt+protiv_nasobrcnt+empty_nasobrcnt));
                System.out.println(";;"+String.valueOf(df.format((za_sobrm2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format((protiv_sobrm2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format((empty_sobrm2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format(((za_sobrm2+protiv_sobrm2+empty_sobrm2)/sum_all)*100).replaceAll(" ", ""))+";;"+String.valueOf(df.format((za_nasobrcnt/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format((protiv_nasobrcnt/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format((empty_nasobrcnt/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format(((za_nasobrcnt+protiv_nasobrcnt+empty_nasobrcnt)/(cnt_all*1.0))*100.0)));
            	System.out.print(name_col+";ПИСЬМОВЕ ОПИТУВАННЯ;"+String.valueOf(df.format(za_m2).replaceAll(" ", ""))+";"+String.valueOf(df.format(protiv_m2).replaceAll(" ", ""))+";"+String.valueOf(df.format(empty_m2).replaceAll(" ", ""))+";"+String.valueOf(df.format(za_m2+protiv_m2+empty_m2).replaceAll(" ", ""))+";;"+String.valueOf(za_cnt)+";"+String.valueOf(protiv_cnt)+";"+String.valueOf(empty_count)+";"+String.valueOf(za_cnt+protiv_cnt+empty_count));
            	System.out.println(";;"+String.valueOf(df.format((za_m2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format((protiv_m2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format((empty_m2/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format(((za_m2+protiv_m2+empty_m2)/sum_all)*100).replaceAll(" ", ""))+";;"+String.valueOf(df.format((za_cnt/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format((protiv_cnt/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format((empty_count/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format(((za_cnt+protiv_cnt+empty_count)/(cnt_all*1.0))*100.0)));
            	System.out.print(name_col+";ВСЬОГО;"+String.valueOf(df.format(za_m2+za_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(protiv_m2+protiv_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(empty_m2+empty_sobrm2).replaceAll(" ", ""))+";"+String.valueOf(df.format(sum_all).replaceAll(" ", ""))+";;"+String.valueOf(za_cnt+za_nasobrcnt)+";"+String.valueOf(protiv_cnt+protiv_nasobrcnt)+";"+String.valueOf(empty_count+empty_nasobrcnt)+";"+String.valueOf(cnt_all));
            	System.out.println(";;"+String.valueOf(df.format(((za_m2+za_sobrm2)/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format(((protiv_m2+protiv_sobrm2)/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format(((empty_m2+empty_sobrm2)/sum_all)*100).replaceAll(" ", ""))+";"+String.valueOf(df.format((sum_all/sum_all)*100).replaceAll(" ", ""))+";;"+String.valueOf(df.format(((za_cnt+za_nasobrcnt)/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format(((protiv_cnt+protiv_nasobrcnt)/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format(((empty_count+empty_nasobrcnt)/(cnt_all*1.0))*100.0))+";"+String.valueOf(df.format((cnt_all/(cnt_all*1.0))*100.0)));
            }
            	
      
     //   	System.out.println("ROW: "+String.valueOf(iLastRow2)+"; COLL: "+String.valueOf(iLastColl2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    
    }
    
    private Double getSum(String file_sum) {
        Double sum = 0.0;
        if (file_sum.length() > 0) {
            try {
            	
            	sum = Double.parseDouble(file_sum.replaceAll(",", "."));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }
        return sum;
    }
}
