/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author viktor
 */
public class ProgrammSetting {

    public HashMap<String, String> LoadCatalogOnLocalFile() throws FileNotFoundException, IOException, ParseException {
        JSONParser parser = new JSONParser();
        
        FileInputStream fis = new FileInputStream("OSBB_Sobranie_settings.txt");
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        Reader reader = new BufferedReader(isr);
        JSONObject obj = (JSONObject) parser.parse(reader);

        HashMap<String, String> hm_setting = new HashMap<String, String>();
        hm_setting.put("folder_path", (String) obj.get("folder_path"));
        hm_setting.put("file_xls", (String) obj.get("file_xls"));
        hm_setting.put("file_xlsx", (String) obj.get("file_xlsx"));
        hm_setting.put("file_ods", (String) obj.get("file_ods"));
        hm_setting.put("file_xml", (String) obj.get("file_xml"));
   
        return hm_setting;
    }
}
