/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author viktor
 */
@Entity
@Table(name = "log_info", catalog = "osbb")
public class LogInfo implements Serializable {

    private static final long serialVersionUID = -468487514032441717L;
    private Integer id_generate, start_step, curent_step;
    private String status_cutent_state, error_msg;
    private Date date_start, date_update, date_finish;
    private Boolean is_finish;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_id") 
    @SequenceGenerator(name="seq_id", sequenceName = "osbb.\"sq_osbb_loginfo\"")
    @Column(name="id_generate", unique=true, nullable=false)
    public Integer getId_generate() {
        return id_generate;
    }

    public void setId_generate(Integer id_generate) {
        this.id_generate = id_generate;
    }

    @Column(name = "curent_step")
    public Integer getCurent_step() {
        return curent_step;
    }

    @Column(name = "status_cutent_state")
    public String getStatus_cutent_state() {
        return status_cutent_state;
    }

    
    public void setStatus_cutent_state(String status_cutent_state) {
        this.status_cutent_state = status_cutent_state;
    }

    public void setCurent_step(Integer curent_step) {
        this.curent_step = curent_step;
    }

    @Column(name = "start_step")
    public Integer getStart_step() {
        return start_step;
    }

    public void setStart_step(Integer start_step) {
        this.start_step = start_step;
    }

    @Column(name = "error_msg")
    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    @Column(name = "date_start")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    @Column(name = "date_update")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getDate_update() {
        return date_update;
    }

    public void setDate_update(Date date_update) {
        this.date_update = date_update;
    }

    @Column(name = "date_finish")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getDate_finish() {
        return date_finish;
    }

    public void setDate_finish(Date date_finish) {
        this.date_finish = date_finish;
    }

    @Column(name = "is_finish")
    public Boolean getIs_finish() {
        return is_finish;
    }

    public void setIs_finish(Boolean is_finish) {
        this.is_finish = is_finish;
    }

}
