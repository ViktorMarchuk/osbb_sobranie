package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tb_osbb_sobranie", catalog = "osbb")
public class Sobranie  implements Serializable {

    private static final long serialVersionUID = -468487514032441717L;
    private Integer id, summ_count;
	private String date_sobranie, number_question, type_question;
    private Date date_add;
    private Double summ_m2;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_id") 
    @SequenceGenerator(name="seq_id", sequenceName = "osbb.\"sq_osbb_sobranie\"")
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "summ_count")
	public Integer getSumm_count() {
		return summ_count;
	}
	public void setSumm_count(Integer summ_count) {
		this.summ_count = summ_count;
	}
	
	@Column(name = "date_sobranie")
	public String getDate_sobranie() {
		return date_sobranie;
	}
	public void setDate_sobranie(String date_sobranie) {
		this.date_sobranie = date_sobranie;
	}
	
	@Column(name = "number_question")
	public String getNumber_question() {
		return number_question;
	}
	public void setNumber_question(String number_question) {
		this.number_question = number_question;
	}
	
	@Column(name = "type_question")
	public String getType_question() {
		return type_question;
	}
	public void setType_question(String type_question) {
		this.type_question = type_question;
	}
	
	@Column(name = "date_add")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	public Date getDate_add() {
		return date_add;
	}
	public void setDate_add(Date date_add) {
		this.date_add = date_add;
	}
	
	@Column(name = "summ_m2")
	public Double getSumm_m2() {
		return summ_m2;
	}
	public void setSumm_m2(Double summ_m2) {
		this.summ_m2 = summ_m2;
	}
}
